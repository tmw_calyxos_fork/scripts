#!/bin/bash
set -euo pipefail
OUR_REMOTE=tmw
fail() { echo "***" >&2; echo "*** Failed to process ${REPO_PROJECT}." >&2; echo "***" >&2; exit 1; };
git() { echo "${REPO_PROJECT}: git" "${@@Q}"; command git "$@"; };
trap fail EXIT
if [ -n "${REPO__upstream:-}" ]; then
  echo "[START]  Working on ${REPO_PROJECT}..."
  #set -x
  REPO_RREV="${REPO_RREV#refs/heads/}"
  [ -z "${REPO__upstream_branch:-}" ] || REPO__upsteam_branch="${REPO__upstream_branch#refs/heads/}"
  git remote add upstream "${REPO__upstream}.git" 2>/dev/null || true
  git fetch --all
  git rebase --abort || true
  changes=0
  git diff --quiet || changes=1
  [ "$changes" -eq 0 ] || git stash
  git checkout --quiet upstream/"${REPO__upstream_branch:-$REPO_RREV}"
  git pull upstream "${REPO__upstream_branch:-$REPO_RREV}"
  git checkout --quiet --track "${OUR_REMOTE}/${REPO__upstream_branch:-$REPO_RREV}" || git checkout --quiet "${REPO__upstream_branch:-$REPO_RREV}" || { echo "STOP, READ"; exit 1; }
# Originally, we merged in upstream changes, which was ugly, but it was something.
#  git pull upstream "${REPO__upstream_branch:-$REPO_RREV}"
# Now, we rebase all of our changes. We may change our mind in the future.
  git rebase upstream/"${REPO__upstream_branch:-$REPO_RREV}"
  git config branch."${REPO__upstream_branch:-$REPO_RREV}".remote "${OUR_REMOTE}"
  git push "${OUR_REMOTE}" HEAD --force
  if [ -n "${REPO__upstream_branch:-}" -a "${REPO__upstream_branch:-}" != "${REPO_RREV}" ]; then
    git config branch."$REPO_RREV".remote "${OUR_REMOTE}"
    git checkout --quiet --track "${OUR_REMOTE}/${REPO_RREV}" || git checkout --quiet "$REPO_RREV"
    git rebase "${REPO__upstream_branch:-$REPO_RREV}"
    git push "${OUR_REMOTE}" HEAD --force
  fi
  [ "$changes" -eq 0 ] || git stash pop
  echo "[FINISH] Finished ${REPO_PROJECT}."
fi
trap - EXIT
