#!/bin/bash
set -euo pipefail
thisdir="$(realpath -es "$(dirname "${BASH_SOURCE[0]}")")"
repo forall -j4 --abort-on-errors -v -c "${thisdir}/integrate-upstream.sh"
