#!/bin/bash
#
# SPDX-FileCopyrightText: 2017, 2020-2021 The LineageOS Project
# SPDX-FileCopyrightText: 2021-2022 The Calyx Institute
#
# SPDX-License-Identifier: Apache-2.0
#

usage() {
    echo "Usage ${0} <newtag> <upstreambranch>"
}

# Verify argument count
if [ "$#" -ne 2 ]; then
    usage
    exit 1
fi

NEWTAG="${1}"
UPSTREAMBRANCH="${2}"

### CONSTANTS ###
readonly script_path="$(cd "$(dirname "$0")";pwd -P)"
readonly vars_path="${script_path}/../vars"

source "${vars_path}/common"

TOP="${script_path}/../../.."

# List of merged repos
PROJECTPATHS=$(cat ${MERGEDREPOS} | grep -w merge | awk '{printf "%s\n", $2}')

echo "#### Branch = ${BRANCH} Upstream branch = ${UPSTREAMBRANCH} ####"
read -p "Press enter to continue."

# Make sure manifest and forked repos are in a consistent state
echo "#### Verifying there are no uncommitted changes on CalyxOS forked AOSP projects ####"
for PROJECTPATH in ${PROJECTPATHS} .repo/manifests; do
    cd "${TOP}/${PROJECTPATH}"
    if [[ -n "$(git status --porcelain)" ]]; then
        echo "Path ${PROJECTPATH} has uncommitted changes. Please fix."
        exit 1
    fi
done
echo "#### Verification complete - no uncommitted changes found ####"

# Iterate over each forked project
for PROJECTPATH in ${PROJECTPATHS}; do
    cd "${TOP}/${PROJECTPATH}"
    echo "#### Pushing upstream for ${PROJECTPATH} ####"
    git push calyx ${NEWTAG}^{commit}:refs/heads/upstream/${UPSTREAMBRANCH}
done
